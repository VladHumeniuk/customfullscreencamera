## Hello ##

This custom camera can capture 30sec video in 720p resolution.

You can give video file path through the Intent String extra. Name of this extra is defined with constant in activity class.


```
#!android

FullscreenCamera.VIDEO_PATH
```


If no file given, new one is created.

Activity returns video file path through the Intent String extra. Name of this extra is defined with constant in activity class.

ButterKnife library is required for this to work.