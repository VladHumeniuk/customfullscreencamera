package com.example.humeniuk.customcamera;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class FullscreenCamera extends Activity implements SurfaceHolder.Callback {

    public static String VIDEO_PATH = "videoPath";

    @Bind(R.id.surface_view)
    SurfaceView mSurfaceview;
    @Bind(R.id.start_panel)
    ViewGroup mStartPanel;
    @Bind(R.id.stop_panel)
    ViewGroup mStopPanel;
    @Bind(R.id.confirm_panel)
    ViewGroup mConfirmPanel;
    @Bind(R.id.captured_video)
    VideoView mVideoView;
    @Bind(R.id.watch_video)
    ImageButton mWatchVideo;
    private File video;
    private SurfaceHolder mHolder;
    private MediaRecorder mMediaRecorder;
    private Camera mCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        ButterKnife.bind(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        mHolder = mSurfaceview.getHolder();
        mHolder.addCallback(this);
        showStartPanel();
        mVideoView.setVisibility(View.GONE);
        checkForFile();
    }

    private void checkForFile() {
        String videoPath = getIntent().getStringExtra(VIDEO_PATH);
        if (!TextUtils.isEmpty(videoPath)) {
            video = new File(videoPath);
        }
    }

    @OnClick(R.id.start)
    protected void onStartClick() {
        initRecorder(mHolder.getSurface());
        mMediaRecorder.start();
        showStopPanel();
    }

    @OnClick(R.id.stop)
    protected void onStopClick() {
        mMediaRecorder.stop();
        mCamera.stopPreview();
        showConfirmPanel();
    }

    @OnClick(R.id.recapture_video)
    protected void onRecaptureClick() {
        video.delete();
        mVideoView.setVisibility(View.GONE);
        mSurfaceview.setVisibility(View.VISIBLE);
        mCamera.startPreview();
        showStartPanel();
    }

    @OnClick(R.id.confirm_video)
    protected void onConfirmClick() {
        Intent result = new Intent();
        result.putExtra(VIDEO_PATH, video.getAbsolutePath());
        setResult(RESULT_OK, result);
        finish();
    }

    @OnClick(R.id.watch_video)
    protected void onWatchVideoClick() {
        mVideoView.setVideoURI(Uri.parse(video.getAbsolutePath()));
        mVideoView.setMediaController(new MediaController(this));
        mSurfaceview.setVisibility(View.INVISIBLE);
        mVideoView.setVisibility(View.VISIBLE);
        mVideoView.start();
        mWatchVideo.setVisibility(View.GONE);
    }

    private void initCamera() {
        if(mCamera == null) {
            mCamera = Camera.open();
        }
        try {
            mCamera.setPreviewDisplay(mHolder);
        } catch (IOException e) {
        }
        mCamera.startPreview();
    }

    private void initRecorder(Surface surface) {

        mCamera.stopPreview();
        mCamera.unlock();

        if(mMediaRecorder == null) {
            mMediaRecorder = new MediaRecorder();
        }
        mMediaRecorder.setPreviewDisplay(surface);
        mMediaRecorder.setCamera(mCamera);
        try {
            File file = createFile();
            mMediaRecorder.setOutputFile(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_720P);
        mMediaRecorder.setProfile(profile);
        mMediaRecorder.setMaxDuration(30000);                   //TODO: make this constant?!
        mMediaRecorder.setVideoFrameRate(15);

        try {
            mMediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void shutdown() {
        try {
            mMediaRecorder.reset();
            mMediaRecorder.release();
            mCamera.release();
        } catch (Exception e) {
        }
        mMediaRecorder = null;
        mCamera = null;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mHolder = holder;
        initCamera();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        shutdown();
    }


    /**
     * Creates file when no filepath was send with Intent;
     * @return File
     * @throws IOException
     */
    private File createFile() throws IOException {
        if (video == null) {
            File dir = getExternalFilesDir(null);
            String filename = "VID_" + System.currentTimeMillis() + ".mp4";
            video = new File(dir, filename);
            video.createNewFile();
        } else if (!video.exists()) {
            video.createNewFile();
        }
        return video;
    }

    private void showStartPanel() {
        mStartPanel.setVisibility(View.VISIBLE);
        mStopPanel.setVisibility(View.GONE);
        mConfirmPanel.setVisibility(View.GONE);
    }

    private void showStopPanel() {
        mStartPanel.setVisibility(View.GONE);
        mStopPanel.setVisibility(View.VISIBLE);
        mConfirmPanel.setVisibility(View.GONE);
    }

    private void showConfirmPanel() {
        mStartPanel.setVisibility(View.GONE);
        mStopPanel.setVisibility(View.GONE);
        mConfirmPanel.setVisibility(View.VISIBLE);
    }
}
